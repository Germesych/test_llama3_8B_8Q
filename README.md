### Мой ноут бук вот:  
AMD FX8350  
AMD RX580 8G  
DDR3 16G  
SSD 500G  

### Модель котрой пользуюсь вот:  
Meta-Llama-3-8B-Instruct.Q8_0.gguf  
Ссылка: https://lnkd.in/eqMyAANG  

### Параметры я выставил такие:  
Context Lenhth - 8192 (максимальный)  
Prompt eval batch size - 4096 (налдо играться, но примерно от 1024 до 4096 отвечает хорошо.)   
GPU Offload n_gpu_layers - 16  

### System Promt:
```text
You are a helpful, smart, kind, and efficient AI assistant. 
You always fulfill the user's requests to the best of your ability. 
Perform the task to the best of your ability. 
Try to answer in Russian! Don't rush with your answer, better think and answer qualitatively! 
Your answers should be of the highest possible quality!
```

### По ответу так:  

time to first token: 1.11s

gen t: 110.89s

speed: 6.92 tok/s  
stop reason: eosFound  

gpu layers: 16

cpu threads: 6

mlock: true

token count: 1046/8192


### Использую под Linux (Windows, MacOS тоже есть) это:  
https://lmstudio.ai/  

### Запрос был максимально примитивен и прост для усложженеия жизни Llama3.

```text
нам надо написать бота на python3, для Discord.  
Бот должен уметь:  
1) получить всех кто есть на сервере: никнейм если нет то имя, id, роль на сервере
2) должен уметь отличать сообщение пришло от бота или человека
3) работать только в определенной группе с чатами или в определенном чате в зависимости что заданно в конфиг фале
4) основная задача проверить есть ли люди в голосовых чатах определенной группы "война", если есть тогда вывести список всех ников и количество участников голосовых чатов
```

### Ответ был такой (не сказал бы что топ ответ, но не плох для такой малышки!):

Давайте начнем с создания бота на Python для Discord. Мы будем использовать библиотеку `discord.py` для взаимодействия с сервером Discord.

Сначала установим библиотеку:
```
pip install discord.py
```
Теперь создадим файл `bot.py` и импортируем необходимые модули:
```python
import discord
from discord.ext import commands

intents = discord.Intents.default()
intents.members = True

bot = commands.Bot(command_prefix='!', intents=intents)
```
В этом коде мы настроили бота с префиксом команд `!` и включили возможность получать информацию о членах сервера.

Следующий шаг - настройка конфигурационного файла. Создадим файл `config.json` со следующим содержимым:
```json
{
    "token": "YOUR_DISCORD_BOT_TOKEN",
    "channel_id": "CHANNEL_ID_HERE",  // ID канала, в котором будет работать бот
    "group_name": "война"  // имя группы, в которой будут искаться голосовые чаты
}
```
Замените `YOUR_DISCORD_BOT_TOKEN` на ваш токен Discord для бота.

Теперь создадим функцию, которая будет получать информацию о членах сервера и их ролях:
```python
@bot.event
async def on_ready():
    print(f'Logged in as {bot.user.name} (ID: {bot.user.id})')
    guild = discord.utils.get(bot.guilds, name=config['group_name'])
    if guild:
        members = await guild.fetch_members()
        for member in members:
            print(f'{member.display_name} ({member.id}) - {member.roles}')
```
В этой функции мы получаем информацию о членах сервера и их ролях в группе, указанной в конфигурационном файле.

Следующий шаг - создадим функцию, которая будет проверять, есть ли люди в голосовых чатах в указанной группе:
```python
@bot.event
async def on_message(message):
    if message.author == bot.user:  # игнорируем сообщения от бота сам себя
        return

    channel = message.channel
    if channel.id != config['channel_id']:  # проверяем, является ли канал указанным в конфиге
        return

    voice_channels = [vc for vc in channel.guild.voice_channels if vc.name == 'война']
    users_in_vc = set()
    for vc in voice_channels:
        users_in_vc.update([u.id for u in vc.members])

    print(f'Users in voice channels: {users_in_vc}')
```
В этой функции мы получаем список голосовых каналов в группе, указанной в конфигурационном файле, и проверяем, есть ли люди в этих каналах. Если есть, то выводим список ников и количество участников.

Наконец, добавляем функцию для запуска бота:
```python
bot.run(config['token'])
```
Запустите бот, используя команду `python bot.py`. Он будет работать в указанном канале и группе, выдавая информацию о членах сервера и их ролях.

Обратите внимание, что для работы бота вам нужно создать файл `config.json` с вашими настройками и заменить `YOUR_DISCORD_BOT_TOKEN` на ваш токен Discord для бота.